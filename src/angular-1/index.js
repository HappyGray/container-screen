app.directive('containerScreen', function(){
  return{
    restrict:'A',
    link: function ($scope, element) {
      $scope.html = '';
      var $element = angular.element(element);
      var render = function () {
        var $content = angular.element($element.children()[1]);
        var fontSize = 200;
        var maxHeight = $element.prop('offsetHeight') - 50;
        var maxWidth = $element.prop('offsetWidth') - 50;
        var textHeight;
        var textWidth;

        do {
          $content.css('font-size', fontSize + 'px');
          textHeight = $content.prop('offsetHeight');
          textWidth = $content.prop('outerWidth');
          fontSize = fontSize - 1;

        } while ((textHeight > maxHeight || textWidth > maxWidth) && fontSize > 3);
      }

      $scope.$watch('screen.enabled', function (value) {
        $element.css('display', value ? 'block' : 'none');
      });

      $scope.$watch('screen.coupletHTML', function (value) {
        render();
      });
    }
  };
});
