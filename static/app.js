((doc, win, $) => {
  $(doc).ready(function () {
    var $con = $("#container");

    /**
     * Обязательная структура: #container > div
     */
    $con.dynamicFontSize();

    $('#btn').click(function () {
      var maxWidth = parseInt($con.css('max-width'));
      $con.css('max-width', (maxWidth - 50) + 'px')
    });
  });
})(document, window, window.jQuery);
