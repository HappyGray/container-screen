((doc, win, $) => {
  function updateFontSize() {
    var fontSize = 200;
    var padding = 0; // todo: не получается реализовать
    var maxWidth = this.width() - padding;
    var maxHeight = this.height() - padding;
    var textHeight;
    var textWidth;

    var child = this.children();

    if (child.length !== 1) {
      throw new Error('Not found children')
    }

    do {
      this.css('font-size', fontSize + 'px');

      textWidth = child.width();
      textHeight = child.height();
      fontSize = fontSize - 1;
    } while ((textHeight > maxHeight || textWidth > maxWidth) && fontSize > 3);
  }

  $.fn.extend({
    dynamicFontSize: function (watch) {
      var self = this;

      updateFontSize.call(self);

      if (typeof watch === 'undefined' || watch === true) {
        var width = this.width();
        var height = this.height();

        setInterval(function () {
          var newWidth = self.width();
          var newWeight = self.height();

          if (newWidth !== width || newWeight !== height) {
            width = self.width();
            height = self.height();

            updateFontSize.call(self);
          }
        }, 50)
      }
    }
  });
})(document, window, window.jQuery);
